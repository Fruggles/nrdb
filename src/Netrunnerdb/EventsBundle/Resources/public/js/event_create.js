delete NRDB.api_url;

$(function () {
	$('#freelocation').on('change', update_address);
	$('#btn-create').on('click', do_create);
	
	var now_array = moment().toArray();
	
	$('#datetimestart-year option').each(function (index, element) {
		if($(element).html() == now_array[0]) {
			$(element).attr("selected", "selected");
		}
	});
	$('#datetimestart-month option').each(function (index, element) {
		if($(element).attr('value') == now_array[1]) {
			$(element).attr("selected", "selected");
		}
	});
	$('#datetimestart-day option').each(function (index, element) {
		if($(element).html() == now_array[2]) {
			$(element).attr("selected", "selected");
		}
	});
});

function do_create(event) {
	$('.has-error').removeClass('has-error');
	$('.alert-danger').remove();
	var msg = [];
	
	var date_components = ['year', 'month', 'day', 'hour', 'minute'];
	var timezone = $('#timezone').val();
	
	var date_array = [];
	$.each(date_components, function (j, date_component) {
		date_array.push($('#datetimestart-'+date_component).val());
	});
	var datestart = moment.tz(date_array, timezone);
	if(!datestart.isValid()) {
		msg.push("The date and time you selected for the start of the event is invalid.");
		$('#row-start').addClass('has-error');
	} else {
		$('#datetimestart').val(datestart.format());
	}
	
	var nonempty_inputs = ['name','location'];
	$.each(nonempty_inputs, function (i, input) {
		if($('#'+input).val() === "") {
			msg.push("A required field is empty.");
			$('#'+input).parent().addClass('has-error');
		}
	});
	
	if($('#address').val() === "") {
		$('#freelocation').parent().addClass('has-error');
		if($('#freelocation').val() === "") {
			msg.push("No address for the event.");
		} else {
			msg.push("The address of the event wasn't recognized.");
		} 
	}
	
	if(msg.length) {
		$.each(msg, function (i, text) {
			$('h1').after('<div class="alert alert-danger" role="alert">'+text+'</div>');
		});
		event.preventDefault();
		return;
	}
	
	
}